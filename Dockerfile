FROM nginx:latest

WORKDIR /usr/share/nginx/html

RUN rm -rf index.html
RUN apt update && apt install git -y

RUN git clone https://gitlab.com/nelia-s/tpdevops.git
RUN mv /usr/share/nginx/html/tpdevops/index.html /usr/share/nginx/html && rm -rf /usr/share/nginx/html/tpdevops

ENV Titre=default
RUN sed -i 's/Titre/'"${Titre}"'/g' /usr/share/nginx/html/index.html